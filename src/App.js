import React from 'react';
import { AppWrapper } from './App.styles';
import Screen from './components/Screen';

function App() {
  return (
    <AppWrapper>
      <Screen />
    </AppWrapper>
  );
}

export default App;
