import styled from 'styled-components';

export const FadeInOut = styled.section`
  &.transition-enter {
      opacity: 0.01;
  }

  &.transition-enter-active {
      opacity: 1;
      transition: opacity 200ms ease-out;
  }
  
  &.transition-leave {
    opacity: 1;
  }

  &.transition-leave-active {
    opacity: 0.01;
    transition: opacity 200ms ease-out;
    }`;