export default {
  siteWidth: '1920px',
  siteHeight: '1080px',
  primary: '#2D4245',
  secondary: '#70C9C4',
  tertiary: '#304143',
  darkGray: '#221f20',
  iconWidth: '55px',
  font1: '"Montserrat", sans-serif'
};

