import styled from 'styled-components';
import theme from '../../theme/theme';

const locations = {
  1: {
    left: '520px',
    top: '295px'
  },
  2: {
    left: '402px',
    top: '832px'
  },
  3: {
    left: '1307px',
    top: '621px'
  },
  4: {
    left: '1336px',
    top: '881px'
  },
  5: {
    left: '855px',
    top: '333px'
  },
  6: {
    left: '1227px',
    top: '401px'
  },
  7: {
    left: '921px',
    top: '751px'
  },
  8: {
    left: '300px',
    top: '457px'
  },
  9: {
    top: '541px',
    left: '1517px'
  }
};

export const FloorplanWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.secondary};
`;

export const FloorplanHeader = styled.h2`
  position: absolute;
  top: 0;
  left: 50px;
  line-height: 111px;
  margin: 0;
  padding: 0;
  color: white;
`;

export const FloorplanBGImage = styled.img`
  position: absolute;
  top: auto;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  z-index: 0;
  pointer-events: none;
  height: auto;
  width: 100%;
`;

export const InfoBtnWrapper = styled.div`
  width: ${theme.iconWidth};
  height: auto;
  position: absolute;
  top: ${props => props.loc ? locations[props.loc].top : '0px'};
  left: ${props => props.loc ? locations[props.loc].left : '0px'};
`;

export const InfoBtn = styled.img`
  width: 100%;
  height: auto;
  cursor: pointer;
`;