import React from 'react';
import { FloorplanWrapper, FloorplanBGImage, FloorplanHeader, InfoBtnWrapper, InfoBtn } from './Floorplan.styles';
import floorplan from '../../theme/floorplan.png';
import eye from '../../theme/teal-eye.svg';
import Popover from '../popover/Popover';

function Floorplan(props) {

  return (
    <FloorplanWrapper>
      <FloorplanBGImage src={floorplan} />
      <FloorplanHeader>The features of a typical PAD Plan</FloorplanHeader>
      
      <InfoBtnWrapper loc={1}><InfoBtn src={eye} onClick={ () => props.handleCarouselState(true, 'floorplan', 0)} /></InfoBtnWrapper>
      <InfoBtnWrapper loc={8}><Popover caption="Heated towel rack to dry beanies + gloves too." /></InfoBtnWrapper>
      <InfoBtnWrapper loc={2}><Popover caption="Storage for boots, shoes + outerwear." /></InfoBtnWrapper>
      <InfoBtnWrapper loc={3}><Popover caption="Coffee table folds out to dining table or work surface." /></InfoBtnWrapper>
      <InfoBtnWrapper loc={4}><Popover caption="Plug your device into the tech/TV wall + relax." /></InfoBtnWrapper>
      <InfoBtnWrapper loc={9}><Popover caption="Sliding door opens to a Juliet balcony for a breath of fresh air." /></InfoBtnWrapper>

      <InfoBtnWrapper loc={5}><InfoBtn src={eye} onClick={ () => props.handleCarouselState(true, 'floorplan', 2)} /></InfoBtnWrapper>
      <InfoBtnWrapper loc={6}><InfoBtn src={eye} onClick={ () => props.handleCarouselState(true, 'floorplan', 3)} /></InfoBtnWrapper>
      <InfoBtnWrapper loc={7}><InfoBtn src={eye} onClick={ () => props.handleCarouselState(true, 'floorplan', 1)} /></InfoBtnWrapper>

    </FloorplanWrapper>
  );
}

export default Floorplan;
