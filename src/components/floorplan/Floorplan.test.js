import React from 'react';
import ReactDOM from 'react-dom';
import Floorplan from './Floorplan';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Floorplan />, div);
  ReactDOM.unmountComponentAtNode(div);
});
