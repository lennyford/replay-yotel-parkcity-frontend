import React from 'react';
import ReactDOM from 'react-dom';
import Saver from './Saver';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Saver />, div);
  ReactDOM.unmountComponentAtNode(div);
});
