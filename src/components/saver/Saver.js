import React from 'react';
import { SaverWrapper, SaverVideo } from './Saver.styles';
import video from '../../theme/Yotel_Final_render_1080.mp4';

function Saver() {

  return (
    <SaverWrapper>
      <SaverVideo autoPlay muted loop >
        <source src={video} type="video/mp4" />
      </SaverVideo>
    </SaverWrapper>
  );
}

export default Saver;
