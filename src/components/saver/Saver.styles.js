import styled from 'styled-components';
import theme from '../../theme/theme';

export const SaverWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.primary};
  z-index: 1000;
`;

export const SaverVideo = styled.video`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  pointer-events: none;
  width: auto;
  height: 100%;
`;
