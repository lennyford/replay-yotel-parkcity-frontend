import styled from 'styled-components';
import theme from '../../theme/theme';

export const CarouselWrapper = styled.div`
  color: white;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.primary};
`;

export const Slide = styled.div`
  height: 100%;
  background-color: ${props => props.color ? props.color : 'white'};
  background-image: url(${props => props.bg ? props.bg : ''});
  background-size: cover;
  min-height: ${theme.siteHeight};
  position: relative;
  img {
    max-width: 100%;
    width: 100%;
    height: auto;
  }
`;

export const Toggle = styled.div`
  position: absolute;
  margin: 0 auto;
  bottom: 50px;
  z-index: 9;
  width: 100%;
`;

export const ToggleBtn = styled.button`
  outline: 0!important;
  cursor: pointer;
  border: 0;
  border-radius: 10px;
  background: ${props => props.active ? theme.secondary : 'white'};
  color: ${props => props.active ? 'white' : theme.primary};
  height: 60px;
  line-height: 60px;
  text-transform: uppercase;
  text-align: center;
  font-size: 21px;
  display: inline-block;
  padding: 0 50px;
  ${props => props.side === 'left' ? 'border-top-right-radius: 0; border-bottom-right-radius: 0;' : ''}
  ${props => props.side === 'right' ? 'border-top-left-radius: 0; border-bottom-left-radius: 0;' : ''}

`;

export const BackBtn = styled.img`
  position: absolute;
  top: 35px;
  right: 140px;
  z-index: 2;
  cursor: pointer;
  width: auto;
  height: 42px;
  background: transparent;
  border: 0;
`;

export const SlideImage = styled.img`
  display: block;
  position: absolute;
  bottom: 0;
`;

export const SlideHeader = styled.h3`
  position: absolute;
  background: ${props => props.context === 'map' ? theme.primary : theme.secondary};
  font-size: 32px;
  top: 0;
  text-indent: 50px;
  line-height: 110px;
  width: 100%;
  margin: 0;
  text-align: left;
`;