import React, { useState } from 'react';
import { CarouselWrapper, Slide, BackBtn, Toggle, SlideImage, ToggleBtn, SlideHeader} from './Carousel.styles';
import Slider from 'react-slick';
import pin from '../../theme/pin.svg';
import CarouselData from '../../carousel';
import { CSSTransitionGroup } from 'react-transition-group';
import { FadeInOut } from '../../theme/Theme.styles';
 

function Carousel(props) {
  
  const slides = CarouselData[props.target];
  const initialSlide = props.index | 0;
  const [showAlt, setShowAlt] = useState(false);
  const [altTextA, setAltTextA] = useState(slides[initialSlide].altTextA);
  const [altTextB, setAltTextB] = useState(slides[initialSlide].altTextB);
  const [currentSlide, setCurrentSlide] = useState(initialSlide);

  const slickConfig = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    initialSlide: initialSlide,
    beforeChange: (...slide) => {
      setCurrentSlide(slide[1]);
      setAltText(slide[1]);
    }
  };

  const toggleAlt = (yepnope) => {
    setShowAlt(yepnope);
  };

  const setAltText = (slide) => {
    if (slides[slide].altTextA) {
      setAltTextA(slides[slide].altTextA);
    }
    if (slides[slide].altTextB) {
      setAltTextB(slides[slide].altTextB);
    }
  };

  return (
    <CarouselWrapper> 

      <BackBtn onClick={ () => props.handleCarouselState(false)} src={pin} alt="Back" />

      {/* something to figure out if this needs to be dynamic */}
      { (currentSlide  === 2 || currentSlide  === 1 || currentSlide  === 3) && props.target === 'floorplan' &&
        <Toggle>
          <ToggleBtn onClick={ () => toggleAlt(false)} side="left" active={!showAlt}> {altTextA} </ToggleBtn>
          <ToggleBtn onClick={ () => toggleAlt(true)} side="right" active={showAlt}> {altTextB} </ToggleBtn>
        </Toggle>
      }

      <Slider {...slickConfig}>
      { slides &&
        slides.map((slide) =>
          <Slide bg={slide.alt} key={slide.name}>
            <CSSTransitionGroup
            transitionName="transition"
            transitionEnter={true}
            transitionLeave={true}
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}>
            { !showAlt &&
              <FadeInOut>
                <SlideImage src={slide.name} alt={slide.title} />
              </FadeInOut>
            }
          </CSSTransitionGroup>
            <SlideHeader context={props.target}>{slide.title}</SlideHeader>
          </Slide>
        )
      }
      </Slider>
    </CarouselWrapper>
  );
}

export default Carousel;
