import React from 'react';
import { HomeWrapper, HomeBG, HomeBGImage, LogoWrapper, YotelLogo, RepImage, LeftColumn, RightColumn, PadWrapper, PadCol, TextButton } from './Home.styles';
import yotelLogo from '../../theme/yotel.svg';
import yotelPadLogo from '../../theme/yotelpad.svg';
import tiles from '../../theme/tiles.svg';
import screen1 from '../../theme/screen1.jpg';
import screen2 from '../../theme/screen2.jpg';
import screen3 from '../../theme/screen3.jpg';

function Home(props) {
  return (
    <HomeWrapper>
      <HomeBG>
        
        <HomeBGImage src={tiles} />

        <LeftColumn>
          <LogoWrapper>
            <YotelLogo src={yotelLogo} alt="Yotel" />
          </LogoWrapper>
          <RepImage onClick={ () => props.handleMapState(true)}>
            <img src={screen1} alt="Around the globe" />
          </RepImage>
          <TextButton onClick={ () => props.handleMapState(true)}>Around the Globe</TextButton>
        </LeftColumn>

        <RightColumn>
          <LogoWrapper>
            <YotelLogo src={yotelPadLogo} alt="YotelPad" />
          </LogoWrapper>

          <PadWrapper>
            <PadCol>
              <RepImage onClick={ () => props.handleFloorplanState(true)}>
                <img src={screen2} alt="PAD Interiors" />
              </RepImage>
              <TextButton onClick={ () => props.handleFloorplanState(true)}>PAD Interiors</TextButton>
            </PadCol>
            <PadCol>
              <RepImage onClick={ () => props.handleAmenityState(true)}>
                <img src={screen3} alt="Social Spaces" />
              </RepImage>
              <TextButton onClick={ () => props.handleAmenityState(true)}>Social Spaces</TextButton>
            </PadCol>
          </PadWrapper>
        </RightColumn>

      </HomeBG>
    </HomeWrapper>
  );
}

export default Home;
