import styled from 'styled-components';
import theme from '../../theme/theme';

export const HomeWrapper = styled.div`
  color: white;
  height: 1040px;
  width: 1880px;
  left: 20px;
  top: 20px;
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
`;

export const LogoWrapper = styled.div`
  text-align: center;
  height: 67px;
  margin-bottom: 80px;
`;

export const YotelLogo = styled.img`
  height: 67px;
  width: auto;
`;

export const HomeBG = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${theme.primary};
`;

export const HomeBGImage = styled.img`
  position: absolute;
  width: 100%;
  height: auto;
  left: 10px;
  top: 170px;
  z-index: 0;
  pointer-events: none;
`;

export const LeftColumn = styled.div`
  width: calc(34% - 1px);
  height: 100%;
  display: block;
  float: left;
  height: 886px;
  border-right: 1px solid white;
  top: 77px;
  position: relative;
`;

export const RightColumn = styled.div`
  width: 66%;
  height: 100%;
  display: block;
  float: left;
  height: 886px;
  top: 77px;
  position: relative;
`;

export const RepImage = styled.div`
  width: 500px;
  height: 500px;
  background-color: white;
  cursor: pointer;
  margin: 0 auto;
`;

export const PadWrapper = styled.div`
  text-align: center;
  padding: 0 30px; 
`;
export const PadCol = styled.div`
  width: 50%;
  float: left;
  text-align: center;
`;

export const TextButton = styled.button`
  outline: 0!important;
  border: 0;
  background: transparent;
  outline: none;
  color: white;
  font-size: 34px;
  line-height: 31px;
  margin-top: 35px;
  font-weight: bold;
`;