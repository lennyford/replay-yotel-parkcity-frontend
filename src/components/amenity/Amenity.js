import React from 'react';
import { AmenityWrapper, AmenityBGImage, AmenityHeader, InfoLabel, InfoBtnWrapper } from './Amenity.styles';
import Popover from '../popover/Popover';
import amenity from '../../theme/yotel-pad-amenity-plan.jpg';

function Amenity(props) {

  return (
    <AmenityWrapper>
      <AmenityBGImage src={amenity} />
      <AmenityHeader>The social spaces at YOTELPAD Park City</AmenityHeader>

      <InfoLabel loc={1} onClick={ () => props.handleCarouselState(true, 'amenity', 0)}>Pool</InfoLabel>
      <InfoLabel loc={2} onClick={ () => props.handleCarouselState(true, 'amenity', 3)} >Fireside Lounge</InfoLabel>
      <InfoLabel loc={3} onClick={ () => props.handleCarouselState(true, 'amenity', 1)} >TV Wall</InfoLabel>
      <InfoLabel loc={4} onClick={ () => props.handleCarouselState(true, 'amenity', 4)} >Games Room</InfoLabel>
      <InfoLabel loc={5} onClick={ () => props.handleCarouselState(true, 'amenity', 2)} >Club Lounge</InfoLabel>
      <InfoLabel loc={6} onClick={ () => props.handleThreeState(true)}>360 View</InfoLabel>
      <InfoLabel loc={7} onClick={ () => props.handleCarouselState(true, 'amenity', 5)} >View Terrace</InfoLabel>
      <InfoBtnWrapper loc={8}><Popover caption="The ski valet takes your gear." label="Ski Valet" /></InfoBtnWrapper>
      <InfoBtnWrapper loc={9}><Popover caption="Pump it up, stretch it out in the Fitness Room." label="Fitness Room" /></InfoBtnWrapper>
      <InfoBtnWrapper loc={10}><Popover caption="Feel the heat in the Steam Room - easy access to the pool." label="Steam Room" /></InfoBtnWrapper>
      <InfoBtnWrapper loc={11}><Popover caption="Find older kids in the Teen Lounge." label="Teen Lounge" /></InfoBtnWrapper>
      <InfoBtnWrapper loc={12}><Popover caption="Drop little ones in the Kids' Zone." label="Kids Zone" /></InfoBtnWrapper>
      <InfoLabel loc={13} className="disabled">East Parkade</InfoLabel>
      <InfoLabel loc={14} className="disabled">West Parkade</InfoLabel>

    </AmenityWrapper>
  );
}

export default Amenity;
