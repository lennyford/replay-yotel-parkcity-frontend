import styled from 'styled-components';
import theme from '../../theme/theme';

const locations = {
  1: {
    top: '269px',
    left: '644px'
  },
  2: {
    left: '660px',
    top: '868px'
  },
  3: {
    left: '904px',
    top: '552px'
  },
  4: {
    left: '878px',
    top: '868px'
  },
  5: {
    left: '1101px',
    top: '619px'
  },
  6: {
    left: '947px',
    top: '347px'
  },
  7: {
    left: '502px',
    top: '970px'
  },
  8: {
    left: '1418px',
    top: '418px'
  },
  9: {
    left: '551px',
    top: '418px'
  },
  10: {
    left: '509px',
    top: '586px'
  },
  11: {
    left: '1101px',
    top: '737px'
  },
  12: {
    left: '1101px',
    top: '837px'
  },
  13: {
    top: '970px',
    left: '943px'
  },
  14: {
    top: '237px',
    left: '493px'
  },
};


export const AmenityWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.secondary};
`;

export const AmenityHeader = styled.h2`
  position: absolute;
  top: 0;
  left: 50px;
  line-height: 111px;
  margin: 0;
  padding: 0;
  color: white;
`;

export const AmenityBGImage = styled.img`
  position: absolute;
  top: auto;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  z-index: 0;
  pointer-events: none;
  height: auto;
  width: 100%;
`;


export const InfoBtn = styled.img`
  width: ${theme.iconWidth};
  height: auto;
  cursor: pointer;
  position: absolute;
  top: ${props => props.loc ? locations[props.loc].top : '0px'};
  left: ${props => props.loc ? locations[props.loc].left : '0px'};
`;

export const InfoLabel = styled.div`
  border-radius: 8px;
  overflow: hidden;
  color: white;
  width: auto;
  border: 2px solid white;
  background-color: ${theme.secondary};
  padding: 0 15px;
  height: 45px;
  line-height: 45px;
  cursor: pointer;
  position: absolute;
  top: ${props => props.loc ? locations[props.loc].top : '0px'};
  left: ${props => props.loc ? locations[props.loc].left : '0px'};
  &.disabled {
    background-color: transparent;
    border: 0;
    cursor: default;
  }
`;

export const InfoBtnWrapper = styled.div`
  width: ${theme.iconWidth};
  height: auto;
  position: absolute;
  top: ${props => props.loc ? locations[props.loc].top : '0px'};
  left: ${props => props.loc ? locations[props.loc].left : '0px'};
`;