import React from 'react';
import ReactDOM from 'react-dom';
import Amenity from './Amenity';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Amenity />, div);
  ReactDOM.unmountComponentAtNode(div);
});
