import React from 'react';
import { ThreeWrapper, ThreeHeader, ThreeFrame, BackBtn } from './ThreeSixty.styles';
import pin from '../../theme/pin.svg';

function ThreeSixty(props) {

  return (
    <ThreeWrapper>
      <ThreeHeader>Float around or lounge by the pool</ThreeHeader>
      <BackBtn onClick={ () => props.handleThreeState(false)} src={pin} alt="Back" />
      <ThreeFrame
        title="Float around or lounge by the pool"
        id="ado-30235"
        src="//www.vroptimal-3dx-assets.com/content/30235?player=true&amp;autoplay=true&amp;referer=https%3A%2F%2Fwww.yotelpadparkcity.com%2Fsocial-spaces%2F"
        frameborder="0"
        width="1920" height="970" 
        webkitallowfullscreen="true" 
        mozallowfullscreen="true" 
        allowfullscreen="true" 
        actual-src="https://player.omnivirt.com/2019/07/24/22/47/52/87ddddcb-42c2-4eac-a656-2e4966fff659/player.html?player=true&amp;autoplay=true&amp;referer=https%3A%2F%2Fwww.yotelpadparkcity.com%2Fsocial-spaces%2F&amp;host=cdn.omnivirt.com&amp;noad=false&amp;videoId=30235&amp;cst=0&amp;streaming=https%3A%2F%2Feu-storage-bitcodin.storage.googleapis.com%2FbitStorage%2F11836_b244ec6935fc37363fe13e529631771a%2F821784_94f3c0d5286f991eda6f816b0e0503d1%2Fm3u8s%2F821784.m3u8%3Fv%3D2%26cb%3D1546977788%3A%3Ahttps%3A%2F%2Feu-storage-bitcodin.storage.googleapis.com%2FbitStorage%2F11836_b244ec6935fc37363fe13e529631771a%2F821784_94f3c0d5286f991eda6f816b0e0503d1%2Fmpds%2F821784.mpd%3Fcb%3D1546977788&amp;version=2.12.65" 
        ado-ready="1" ado-inview="1" ado-preroll="1" />
    </ThreeWrapper>
  );
}

export default ThreeSixty;
