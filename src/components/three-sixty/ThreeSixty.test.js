import React from 'react';
import ReactDOM from 'react-dom';
import ThreeSixty from './ThreeSixty';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ThreeSixty />, div);
  ReactDOM.unmountComponentAtNode(div);
});
