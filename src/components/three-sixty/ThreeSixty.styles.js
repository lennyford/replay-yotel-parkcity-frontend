import styled from 'styled-components';
import theme from '../../theme/theme';

export const ThreeWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.primary};
`;

export const ThreeHeader = styled.h3`
  position: absolute;
  background: ${theme.secondary};
  font-size: 32px;
  top: 0;
  text-indent: 50px;
  line-height: 110px;
  width: 100%;
  margin: 0;
  text-align: left;
  color: white;
`;

export const ThreeFrame = styled.iframe`
  margin-top: 110px;
  position: relative;
  border: 0;
`;

export const BackBtn = styled.img`
  position: absolute;
  top: 35px;
  right: 140px;
  z-index: 2;
  cursor: pointer;
  width: auto;
  height: 42px;
  background: transparent;
  border: 0;
`;