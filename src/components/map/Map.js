import React from 'react';
import { MapWrapper, YotelLogo, MapBGImage } from './Map.styles';
import map from '../../theme/map.png';
import yotelLogo from '../../theme/yotel.svg';
import Landed from '../ui/landed/Landed';
import Takeoff from '../ui/takeoff/Takeoff';

function Map(props) {

  const handleClick = (index) => {
    props.handleCarouselState(true, 'map', index);
  }

  return (
    <MapWrapper>
      <MapBGImage src={map} />
      <YotelLogo src={yotelLogo} alt="Yotel" />
      <Takeoff />
      <Landed handleClick={handleClick} />
    </MapWrapper>
  );
}

export default Map;
