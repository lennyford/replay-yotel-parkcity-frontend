import styled from 'styled-components';
import theme from '../../theme/theme';

export const MapWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${theme.primary};
`;

export const YotelLogo = styled.img`
  height: 67px;
  width: auto;
  position: absolute;
  left: 50px;
  top: 60px;
`;

export const MapBGImage = styled.img`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  z-index: 0;
  pointer-events: none;
`;
