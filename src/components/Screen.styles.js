import styled from 'styled-components';
import theme from '../theme/theme';

export const ScreenWrapper = styled.div`
  height: ${theme.siteHeight};
  width: ${theme.siteWidth};
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  background-color: ${theme.darkGray};
  overflow: hidden;
`;

export const ScreenBounds = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

export const CloseBtn = styled.img`
  position: absolute;
  top: 35px;
  right: 50px;
  z-index: 999;
  cursor: pointer;
  width: auto;
  height: 42px;
  background: transparent;
  border: 0;
`;