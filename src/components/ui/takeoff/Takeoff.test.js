import React from 'react';
import ReactDOM from 'react-dom';
import Takeoff from './Takeoff';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Takeoff />, div);
  ReactDOM.unmountComponentAtNode(div);
});
