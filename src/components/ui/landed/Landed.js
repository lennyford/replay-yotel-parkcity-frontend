import React from 'react';
import { Locations } from './Landed.styles';
import './Landed.css';

function Landed(props) {

  const handleClick = (index) => {
    props.handleClick(index);
  }

  return (
    <Locations>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1352.39 745.04" className="landed">
        <defs>
        </defs>
        <g id="Layer_2">
          <g id="Layer_1-2">
            <g id="Heathrow" onClick={() => handleClick(11)} className="hitArea">
              <path className="cls-1" d="M458.76 46.87c0-3.34 1.75-6.06 3.89-6.06H579c2.14 0 3.89 2.72 3.89 6.06v24.25c0 3.34-1.75 6.06-3.89 6.06H462.65c-2.14 0-3.89-2.72-3.89-6.06z" /><text transform="translate(475.23 61.94)" fill="#fff" fontFamily="Montserrat-Regular,Montserrat" fontSize="10">Lo<tspan className="cls-3" x="12.16" y="0">n</tspan>
                <tspan x="18.97" y="0" letterSpacing="0">d</tspan>
                <tspan x="25.77" y="0">on</tspan>
                <tspan x="38.81" y="0" letterSpacing="-.03em"> </tspan>
                <tspan className="cls-3" x="41.11" y="0">H</tspan>
                <tspan x="49.28" y="0">eat</tspan>
                <tspan x="65.28" y="0" letterSpacing=".01em">h</tspan>
                <tspan x="72.11" y="0" letterSpacing="0">r</tspan>
                <tspan x="76.1" y="0">ow</tspan>
              </text></g>
            <g id="Gatwick" onClick={() => handleClick(10)} className="hitArea">
              <path className="cls-1" d="M458.76 131.63c0-3.34 1.95-6.06 4.35-6.06h114.23c2.4 0 4.35 2.72 4.35 6.06v24.24c0 3.34-2 6.07-4.35 6.07H463.11c-2.4 0-4.35-2.73-4.35-6.07z" /><text className="cls-8" transform="translate(475.51 146.96)">Lo<tspan className="cls-9" x="13.27" y="0">n</tspan>
                <tspan className="cls-10" x="20.7" y="0">d</tspan>
                <tspan x="28.11" y="0">on</tspan>
                <tspan x="42.34" y="0" letterSpacing="-.03em"> </tspan>
                <tspan className="cls-12" x="44.85" y="0">Ga</tspan>
                <tspan className="cls-13" x="59.72" y="0">t</tspan>
                <tspan x="64.18" y="0" letterSpacing="0">w</tspan>
                <tspan className="cls-10" x="73.73" y="0">i</tspan>
                <tspan className="cls-15" x="76.69" y="0">c</tspan>
                <tspan x="82.87" y="0">k</tspan>
              </text></g>
            <g id="New_York" onClick={() => handleClick(8)} className="hitArea">
              <path className="cls-1" d="M224.83 330.86c0-3.34 2.35-6.06 5.23-6.06h88.12c2.88 0 5.22 2.72 5.22 6.06v24.24c0 3.34-2.34 6.06-5.22 6.06h-88.12c-2.88 0-5.23-2.72-5.23-6.06z" /><text className="cls-8" transform="translate(247.91 346.19)">
                <tspan className="cls-9">N</tspan>
                <tspan className="cls-12" x="8.91" y="0">e</tspan>
                <tspan className="cls-16" x="15.5" y="0">w</tspan>
                <tspan className="cls-17" x="25.03" y="0"> </tspan>
                <tspan x="27.75" y="0">Yo</tspan>
                <tspan className="cls-18" x="41.52" y="0">r</tspan>
                <tspan className="cls-12" x="45.86" y="0">k</tspan>
              </text></g>
            <g id="Boston" onClick={() => handleClick(9)} className="hitArea">
              <path className="cls-1" d="M237.86 260.41a6 6 0 015.88-6.06h74.58a6 6 0 015.88 6.06v24.25a6 6 0 01-5.88 6.06h-74.58a6 6 0 01-5.88-6.06z" /><text className="cls-8" transform="translate(261.52 275.74)">
                <tspan className="cls-19">B</tspan>
                <tspan x="8.18" y="0">oston</tspan>
              </text></g>
            <g id="Amsterdam" onClick={() => handleClick(1)} className="hitArea">
              <path className="cls-1" d="M760.62 166.85c0-3.34 2-6.06 4.34-6.06h143.9c2.39 0 4.34 2.72 4.34 6.06v24.25c0 3.34-2 6.06-4.34 6.06H765c-2.39 0-4.34-2.72-4.34-6.06z" /><text className="cls-8" transform="translate(779.04 182.18)">A<tspan x="7.82" y="0" letterSpacing="-.01em">m</tspan>
                <tspan x="19.31" y="0">ste</tspan>
                <tspan x="35.66" y="0" letterSpacing="-.01em">r</tspan>
                <tspan className="cls-22" x="39.97" y="0">da</tspan>
                <tspan className="cls-19" x="53.81" y="0">m</tspan>
                <tspan x="65.34" y="0" letterSpacing="-.02em"> </tspan>
                <tspan x="67.94" y="0">Sc</tspan>
                <tspan className="cls-24" x="80.79" y="0">h</tspan>
                <tspan x="88.21" y="0" letterSpacing="0">i</tspan>
                <tspan x="91.15" y="0">p</tspan>
                <tspan className="cls-26" x="98.55" y="0">h</tspan>
                <tspan x="105.99" y="0" letterSpacing="0">o</tspan>
                <tspan className="cls-12" x="112.81" y="0">l</tspan>
              </text></g>
            <g id="Paris" onClick={() => handleClick(5)} className="hitArea">
              <path className="cls-1" d="M597.76 605.46c0-3.34 2.16-6.06 4.81-6.06h171.65c2.66 0 4.82 2.72 4.82 6.06v24.25c0 3.34-2.16 6.06-4.82 6.06H602.57c-2.65 0-4.81-2.72-4.81-6.06z" /><text className="cls-8" transform="translate(625.98 620.79)">Pa<tspan className="cls-18" x="14.27" y="0">r</tspan>
                <tspan className="cls-10" x="18.61" y="0">i</tspan>
                <tspan x="21.57" y="0">s </tspan>
                <tspan className="cls-18" x="29.76" y="0">C</tspan>
                <tspan className="cls-15" x="37.57" y="0">h</tspan>
                <tspan className="cls-22" x="45" y="0">arles</tspan>
                <tspan x="70.67" y="0" letterSpacing="-.01em"> </tspan>
                <tspan className="cls-10" x="73.4" y="0">d</tspan>
                <tspan x="80.82" y="0">e</tspan>
                <tspan x="87.41" y="0" letterSpacing="-.01em"> </tspan>
                <tspan x="90.11" y="0">Gaul</tspan>
                <tspan className="cls-26" x="115.26" y="0">l</tspan>
                <tspan className="cls-12" x="118.25" y="0">e</tspan>
              </text></g>
            <g id="Singapore" onClick={() => handleClick(4)} className="hitArea">
              <path className="cls-1" d="M1182 714.73c0-3.34 2-6.06 4.46-6.06h161.5c2.47 0 4.47 2.72 4.47 6.06V739c0 3.34-2 6.06-4.47 6.06h-161.5c-2.46 0-4.46-2.72-4.46-6.06z" /><text className="cls-8" transform="translate(1209.09 729.79)">Si<tspan className="cls-30" x="9.64" y="0">n</tspan>
                <tspan className="cls-18" x="17.1" y="0">g</tspan>
                <tspan x="24.55" y="0">a</tspan>
                <tspan className="cls-24" x="30.98" y="0">p</tspan>
                <tspan x="38.41" y="0">o</tspan>
                <tspan className="cls-19" x="45.25" y="0">r</tspan>
                <tspan x="49.58" y="0">e O</tspan>
                <tspan x="68.19" y="0" letterSpacing="0">r</tspan>
                <tspan className="cls-9" x="72.53" y="0">ch</tspan>
                <tspan className="cls-22" x="86.14" y="0">ard</tspan>
                <tspan x="104.35" y="0" letterSpacing="-.03em"> </tspan>
                <tspan className="cls-24" x="106.85" y="0">R</tspan>
                <tspan x="114.77" y="0">d</tspan>
              </text></g>
            <g id="Singapore_Airport" onClick={() => handleClick(3)} className="hitArea">
              <path className="cls-1" d="M1182 622.55c0-3.34 1.84-6.06 4.09-6.06h162.21c2.25 0 4.09 2.72 4.09 6.06v24.24c0 3.34-1.84 6.06-4.09 6.06h-162.25c-2.25 0-4.09-2.72-4.09-6.06z" /><text className="cls-8" transform="translate(1204.67 637.61)">Si<tspan className="cls-30" x="9.64" y="0">n</tspan>
                <tspan className="cls-18" x="17.1" y="0">g</tspan>
                <tspan x="24.55" y="0">a</tspan>
                <tspan className="cls-24" x="30.98" y="0">p</tspan>
                <tspan x="38.41" y="0">o</tspan>
                <tspan className="cls-19" x="45.25" y="0">r</tspan>
                <tspan x="49.58" y="0">e </tspan>
                <tspan x="59.03" y="0" letterSpacing="-.01em">C</tspan>
                <tspan className="cls-15" x="66.82" y="0">h</tspan>
                <tspan x="74.25" y="0">a</tspan>
                <tspan className="cls-26" x="80.69" y="0">n</tspan>
                <tspan className="cls-18" x="88.13" y="0">g</tspan>
                <tspan className="cls-13" x="95.58" y="0">i</tspan>
                <tspan className="cls-17" x="98.55" y="0"> </tspan>
                <tspan className="cls-12" x="101.28" y="0">Air</tspan>
                <tspan className="cls-34" x="116.41" y="0">p</tspan>
                <tspan className="cls-12" x="123.81" y="0">o</tspan>
                <tspan className="cls-19" x="130.66" y="0">r</tspan>
                <tspan x="134.99" y="0">t</tspan>
              </text></g>
              <g id="Istanbul" onClick={() => handleClick(2)} className="hitArea">
                <path className="cls-1" d="M870.43 410.93c0-3.34 2.33-6.06 5.19-6.06H1005c2.86 0 5.19 2.72 5.19 6.06v24.25c0 3.34-2.33 6.06-5.19 6.06H875.62c-2.86 0-5.19-2.72-5.19-6.06z" /><text className="cls-8" transform="translate(897.67 426.26)">
                <tspan className="cls-15">I</tspan>
                <tspan x="3.34" y="0">st</tspan>
                <tspan className="cls-35" x="13.1" y="0">a</tspan>
                <tspan className="cls-15" x="19.56" y="0">n</tspan>
                <tspan className="cls-10" x="26.99" y="0">b</tspan>
                <tspan x="34.41" y="0">ul</tspan>
                <tspan x="44.69" y="0" letterSpacing="-.03em"> </tspan>
                <tspan x="47.17" y="0">Air</tspan>
                <tspan className="cls-34" x="62.31" y="0">p</tspan>
                <tspan className="cls-12" x="69.71" y="0">o</tspan>
                <tspan className="cls-19" x="76.55" y="0">r</tspan>
                <tspan x="80.89" y="0">t</tspan>
              </text>
            </g>
            <g id="Edinburgh" onClick={() => handleClick(0)} className="hitArea">
              <path className="cls-1" d="M615.27 6.06c0-3.34 2.4-6.06 5.35-6.06h96.89c3 0 5.35 2.72 5.35 6.06v24.25c0 3.34-2.4 6.06-5.35 6.06h-96.89c-3 0-5.35-2.72-5.35-6.06z" /><text className="cls-8" transform="translate(636.86 21.39)">E<tspan className="cls-9" x="7.3" y="0">d</tspan>
                <tspan className="cls-10" x="14.74" y="0">i</tspan>
                <tspan className="cls-15" x="17.7" y="0">n</tspan>
                <tspan className="cls-35" x="25.13" y="0">b</tspan>
                <tspan x="32.54" y="0">u</tspan>
                <tspan x="39.89" y="0" letterSpacing="0">r</tspan>
                <tspan className="cls-18" x="44.21" y="0">g</tspan>
                <tspan x="51.66" y="0">h</tspan>
              </text></g>
            <g id="San_Francisco" onClick={() => handleClick(7)} className="hitArea">
              <path className="cls-1" d="M0 467.9c0-3.34 2.16-6.06 4.82-6.06h108.77c2.66 0 4.82 2.72 4.82 6.06v24.24c0 3.34-2.16 6.06-4.82 6.06H4.82c-2.66 0-4.82-2.72-4.82-6.06z" /><text className="cls-8" transform="translate(21.22 483.23)">Sa<tspan className="cls-24" x="13.15" y="0">n</tspan>
                <tspan x="20.56" y="0"> Fra</tspan>
                <tspan className="cls-15" x="41.14" y="0">nc</tspan>
                <tspan className="cls-10" x="54.76" y="0">i</tspan>
                <tspan x="57.71" y="0">s</tspan>
                <tspan className="cls-16" x="63.05" y="0">c</tspan>
                <tspan x="69.12" y="0">o</tspan>
              </text></g>
            <g id="Washington" onClick={() => handleClick(6)} className="hitArea">
              <path className="cls-1" d="M398.6 483.58c0-3.35 1.82-6.06 4.06-6.06h106.27c2.24 0 4.05 2.71 4.05 6.06v24.25c0 3.34-1.81 6.06-4.05 6.06H402.66c-2.24 0-4.06-2.72-4.06-6.06z" /><text className="cls-8" transform="translate(412.08 498.91)">
                <tspan className="cls-9">W</tspan>
                <tspan className="cls-34" x="12.16" y="0">a</tspan>
                <tspan x="18.61" y="0">s</tspan>
                <tspan x="23.95" y="0" letterSpacing=".01em">h</tspan>
                <tspan className="cls-10" x="31.4" y="0">i</tspan>
                <tspan className="cls-30" x="34.35" y="0">n</tspan>
                <tspan x="41.81" y="0">gto</tspan>
                <tspan x="60.56" y="0" letterSpacing=".01em">n</tspan>
                <tspan x="68.03" y="0" letterSpacing="-.03em"> </tspan>
                <tspan x="70.51" y="0" letterSpacing="0">D</tspan>
                <tspan x="79.58" y="0">C</tspan>
              </text></g>
          </g>
        </g>
      </svg>
    </Locations >
  );
}

export default Landed;
