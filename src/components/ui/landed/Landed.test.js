import React from 'react';
import ReactDOM from 'react-dom';
import Landed from './Landed';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Landed />, div);
  ReactDOM.unmountComponentAtNode(div);
});
