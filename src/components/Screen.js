import React, { useState } from 'react';
import { ScreenWrapper, ScreenBounds, CloseBtn } from './Screen.styles';
import Home from './home/Home';
import Map from './map/Map';
import Floorplan from './floorplan/Floorplan';
import Amenity from './amenity/Amenity';
import Saver from './saver/Saver';
import Carousel from './carousel/Carousel';
import ThreeSixty from './three-sixty/ThreeSixty';
import { CSSTransitionGroup } from 'react-transition-group';
import { FadeInOut } from '../theme/Theme.styles';
import { useInterval } from '../hooks';
import home from '../theme/home.svg';

function Screen() {

  const [mapActive, setMapActive] = useState(false);
  const [floorplanActive, setFloorplanActive] = useState(false);
  const [amenityActive, setAmenityActive] = useState(false);
  const [saverActive, setSaverActive] = useState(false);
  const [threeActive, setThreeActive] = useState(false);
  const [carouselActive, setCarouselActive] = useState(false);
  const [carouselIndex, setCarouselIndex] = useState(0);
  const [carouselTarget, setCarouselTarget] = useState('');
  const [count, setCount] = useState(0);
  
  const handleSaverState = (state) => {
    setSaverActive(state);
  }
  
  const handleMapState = (state) => {
    setMapActive(state);
  }

  const handleFloorplanState = (state) => {
    setFloorplanActive(state);
  }

  const handleAmenityState = (state) => {
    setAmenityActive(state);
  }

  const handleThreeState = (state) => {
    setThreeActive(state);
  }

  const handleCarouselState = (state, target, index = 0) => {
    setCarouselTarget(target);
    setCarouselIndex(index);
    setCarouselActive(state);
  }

  const handleResetState = () => {
    setSaverActive(false);
    setCarouselActive(false);
    setAmenityActive(false);
    setFloorplanActive(false);
    setMapActive(false);
    setThreeActive(false);
  }

  useInterval(() => {
    if (count >= 100 && !saverActive) {
      setSaverActive(true);
    } else if (count < 10) {
      setCount(count => count + 1);
    }
  }, 1000);

  const resetInterval = () => {
    setSaverActive(false);
    setCount(0);
  }


  return (
    <ScreenWrapper onClick={() => resetInterval()}>
      <ScreenBounds>
        {/* level 1 */}
        <Home 
          handleMapState={handleMapState} 
          handleFloorplanState={handleFloorplanState} 
          handleAmenityState={handleAmenityState}
        />

        <CSSTransitionGroup
          transitionName="transition"
          transitionEnter={true}
          transitionLeave={true}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>
          {/* level 2 */}
          { mapActive &&
            <FadeInOut>
              <Map 
                handleMapState={handleMapState} 
                handleCarouselState={handleCarouselState}
                handleResetState={handleResetState} 
              />
            </FadeInOut>
          }
          { floorplanActive &&
            <FadeInOut>
              <Floorplan 
                handleFloorplanState={handleFloorplanState} 
                handleCarouselState={handleCarouselState} 
                handleResetState={handleResetState} 
              />
            </FadeInOut>
          }
          { amenityActive &&
            <FadeInOut>
              <Amenity 
                handleAmenityState={handleAmenityState} 
                handleCarouselState={handleCarouselState} 
                handleThreeState={handleThreeState} 
                handleResetState={handleResetState}
              />
            </FadeInOut>
          }
          {/* level 3: carousel & 3 */}
          { carouselActive &&
            <FadeInOut>
              <Carousel 
                handleCarouselState={handleCarouselState} 
                target={carouselTarget}
                index={carouselIndex}
                />
            </FadeInOut>
          }
          { threeActive &&
            <FadeInOut>
              <ThreeSixty handleThreeState={handleThreeState} />
            </FadeInOut>
          }
          {/* level 4: saver */}
          { saverActive &&
            <FadeInOut>
              <Saver handleSaverState={handleSaverState} />
            </FadeInOut>
          }
          { ( mapActive || floorplanActive || amenityActive ) &&
            <FadeInOut>
              <CloseBtn onClick={ () => handleResetState()} src={home} alt="Home" />
            </FadeInOut>
          }
        </CSSTransitionGroup>

      </ScreenBounds>
    </ScreenWrapper>
  );
}

export default Screen;