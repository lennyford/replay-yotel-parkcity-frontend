import React, { useRef, useState, useEffect } from 'react';
import { InfoBtn, InfoCaption, PopoverWrapper } from './Popover.styles';
import info from '../../theme/teal-info.svg';
import { CSSTransitionGroup } from 'react-transition-group';
import { FadeInOut } from '../../theme/Theme.styles';

function Popover(props) {

  const [showPopover, setShowPopover] = useState(false);
  const wrapperRef = useRef(null);

  // close others if clicked outside
  function handleClickOutside(event) {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setShowPopover(false);
    }
  }

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  const handleClick = () => {
    setShowPopover(!showPopover);
  }
  
  return (
    <PopoverWrapper ref={wrapperRef}>
      <CSSTransitionGroup
        transitionName="transition"
        transitionEnter={true}
        transitionLeave={true}
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}>
        { showPopover &&
          <FadeInOut>
            <InfoCaption>{ props.caption }</InfoCaption>
          </FadeInOut>
        }
      </CSSTransitionGroup>
      <InfoBtn src={info} onClick={() => handleClick()} />
    </PopoverWrapper>
  );
}

export default Popover;
