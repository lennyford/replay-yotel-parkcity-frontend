import styled from 'styled-components';
import theme from '../../theme/theme';

export const PopoverWrapper = styled.div`
  position: relative;
  height: ${theme.iconWidth};
`;

export const InfoBtn = styled.img`
  display: block;
  width: ${theme.iconWidth};
  height: auto;
  cursor: pointer;
`;

export const InfoCaption = styled.div`
  font-family: ${theme.font1};
  display: inline-block;
  position: absolute;
  bottom: 75px;
  left: -100px;
  border-radius: 6px;
  overflow: hidden;
  text-align: center;
  font-size: 24px;
  line-height: 36px;
  background-color: ${theme.secondary};
  color: white;
  padding: 5px 12px;
  border: 2px solid white;
  width: 271px;
`;