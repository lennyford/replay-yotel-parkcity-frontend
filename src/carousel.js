// map
import edinburgh from './theme/slides/map/edinburgh.jpg';
import amsterdam_schipol from './theme/slides/map/amsterdam_schipol.jpg';
import istanbul from './theme/slides/map/istanbul.jpg';
import singapore_airport from './theme/slides/map/singapore_airport.jpg';
import singapore from './theme/slides/map/singapore.jpg';
import paris from './theme/slides/map/paris.jpg';
import washington from './theme/slides/map/washington.jpg';
import sanfran from './theme/slides/map/sanfran.jpg';
import newyork from './theme/slides/map/newyork.jpg';
import boston from './theme/slides/map/boston.jpg';
import gatwick from './theme/slides/map/gatwick.jpg';
import heathrow from './theme/slides/map/heathrow.jpg';

// floorplan
import bathroom from './theme/slides/floorplan/bathroom.jpg';
import storage_closed from './theme/slides/floorplan/storage_closed.jpg';
import storage_open from './theme/slides/floorplan/storage_open.jpg';
import desk_down from './theme/slides/floorplan/desk_down.jpg';
import desk_bunks from './theme/slides/floorplan/desk_bunks.jpg';
import livingroom_sofa from './theme/slides/floorplan/livingroom_sofa.jpg';
import livingroom_bed from './theme/slides/floorplan/livingroom_bed.jpg';

// amenity
import pool from './theme/slides/amenity/pool.jpg';
import deck from './theme/slides/amenity/deck.jpg';
import lounge from './theme/slides/amenity/lounge.jpg';
import tv_wall from './theme/slides/amenity/tv_wall.jpg';
import games_room from './theme/slides/amenity/games_room.jpg';
import fireplace from './theme/slides/amenity/fireplace.jpg';

const carouselData = {
    "map": [ 
        {
            "name": edinburgh,
            "title": "Edinburgh"
        },
        {
            "name": amsterdam_schipol,
            "title": "Amsterdam Schipol"
        },
        {
            "name": istanbul,
            "title": "Istanbul"
        },
        {
            "name": singapore_airport,
            "title": "Singapore Changi Airport"
        },
        {
            "name": singapore,
            "title": "Singapore Orchard Rd"
        },
        {
            "name": paris,
            "title": "Paris Charles de Gaulle"
        },
        {
            "name": washington,
            "title": "Washington DC"
        },
        {
            "name": sanfran,
            "title": "San Francisco"
        },
        {
            "name": newyork,
            "title": "New York"
        },
        {
            "name": boston,
            "title": "Boston"
        },
        {
            "name": gatwick,
            "title": "London Gatwick"
        },
        {
            "name": heathrow,
            "title": "London Heathrow"
        }
    ],
    "floorplan": [ 
        {
            "name": bathroom,
            "title": "Frameless glass shower with Monsoon rain system + spray wand",
            "alt": bathroom
        },
        {
            "name": storage_closed,
            "title": "Wall conceals storage for outdoor gear",
            "alt": storage_open,
            "altTextA": "Closed",
            "altTextB": "Open"
        },
        {
            "name": desk_down,
            "title": "A desk by day. Bunk beds after dark",
            "alt": desk_bunks,
            "altTextA": "Relax",
            "altTextB": "Sleep"
        },
        {
            "name": livingroom_sofa,
            "title": "Roomy sofa becomes comfy bed",
            "alt": livingroom_bed,
            "altTextA": "Relax",
            "altTextB": "Sleep"
        },
    ],
    "amenity": [
        {
            "name": pool,
            "title": "Hang out poolside and fireside in your Backyard"
        },
        {
            "name": tv_wall,
            "title": "Watch the game and check the snow forecast in the Club Lounge"
        },
        {
            "name": lounge,
            "title": "Grab a snack and drink at the Express Grocery and relax in the Club Lounge"
        },
        {
            "name": fireplace,
            "title": "Unwind with drinks in the Fireside Lounge"
        },
        {
            "name": games_room,
            "title": "Shoot pool or play PAC-MAN in the Games Room"
        },
        {
            "name": deck,
            "title": "Enjoy the valley vista from your View Terrace"
        }
    ]
}

export default carouselData;